BEGIN PLOT /BABAR_2010_I867611/d01-x01-y01
Title=Helicity angle for $D_1(2420)^0$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I867611/d01-x01-y02
Title=Helicity angle for $D_2^*(2420)^0$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I867611/d01-x01-y03
Title=Helicity angle for $D_0(2550)^0$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I867611/d01-x01-y04
Title=Helicity angle for $D_1^*(2600)^0$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I867611/d01-x01-y05
Title=Helicity angle for $D_3^*(2750)^0$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
END PLOT
