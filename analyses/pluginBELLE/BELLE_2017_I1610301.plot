BEGIN PLOT /BELLE_2017_I1610301/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $\Upsilon(3S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d01-x01-y03
Title=$\pi^+\pi^-$ mass in $\Upsilon(4S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d01-x01-y04
Title=$\pi^+\pi^-$ mass in $\Upsilon(4S)\to\Upsilon(2S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2017_I1610301/d02-x01-y01
Title=$\pi^+$ helicity angle in $\Upsilon(2S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\pi^+}$
XLabel=$\cos\theta_{\pi^+}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d02-x01-y02
Title=$\pi^+$ helicity angle in $\Upsilon(3S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\pi^+}$
XLabel=$\cos\theta_{\pi^+}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d02-x01-y03
Title=$\pi^+$ helicity angle in $\Upsilon(4S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\pi^+}$
XLabel=$\cos\theta_{\pi^+}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1610301/d02-x01-y04
Title=$\pi^+$ helicity angle in $\Upsilon(4S)\to\Upsilon(2S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\pi^+}$
XLabel=$\cos\theta_{\pi^+}$
LogY=0
END PLOT

