
BEGIN PLOT /ZEUS_2008_I780108/d11-x01-y01
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d11-x01-y02
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d12-x01-y01
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d12-x01-y02
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, positrons
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d13-x01-y01
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d13-x01-y02
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d14-x01-y01
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d14-x01-y02
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, positrons
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d15-x01-y01
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d15-x01-y02
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d16-x01-y01
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d16-x01-y02
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, positrons
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d17-x01-y01
LogX=1
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d17-x01-y02
LogX=1
Title=Differential unpolarized cross section for inclusive jet production as a function of the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d18-x01-y01
LogX=1
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d18-x01-y02
LogX=1
Title=Differential unpolarized cross section for two jet production as a functionof the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d19-x01-y01
LogX=1
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, electrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d19-x01-y02
LogX=1
Title=Differential unpolarized cross section for three jet production as a function of the jet pseudorapidity, positrons
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d20-x01-y01
LogX=1
Title=Differential unpolarized inclusive-jet cross section as a function of X, electrons
XLabel=x
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d20-x01-y02
LogX=1
Title=Differential unpolarized inclusive-jet cross section as a function of X, positrons
XLabel=x
END PLOT



BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y01
LogX=1
Title=Integrated unpolarized inclusive jet cross section in the given kinemetical region, E- P --> NUE JET X
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y02
LogX=1
Title=Integrated unpolarized inclusive jet cross section in the given kinemetical region, E+ P --> NUEBAR JET X
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y03
LogX=1
Title=Integrated unpolarized dijet cross section in the given kinemetical region, E- P --> NUE JET JET X
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y04
LogX=1
Title=Integrated unpolarized dijet cross section in the given kinemetical region, E+ P --> NUEBAR JET JET X
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y05
LogX=1
Title=Integrated unpolarized trijet cross section in the given kinemetical region, E- P --> NUE JET JET JET X
XLabel=$x$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y06
LogX=1
Title=Integrated unpolarized trijet cross section in the given kinemetical region, E+ P --> NUEBAR JET JET JET X
XLabel=$x$
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d22-x01-y01
Title=Differential unpolarized dijet cross section as a function of the dijet mass.
XLabel=$M^{jj},GeV$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d22-x01-y02
Title=Differential unpolarized dijet cross section as a function of the dijet mass.
XLabel=$M^{jj},GeV$
END PLOT




BEGIN PLOT /ZEUS_2008_I780108/d23-x01-y01
Title=Differential unpolarized trijet cross section as a function of the trijet mass.
XLabel=$M^{jjj},GeV$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d23-x01-y02
Title=Differential unpolarized trijet cross section as a function of the trijet mass.
XLabel=$M^{jjj},GeV$
END PLOT


