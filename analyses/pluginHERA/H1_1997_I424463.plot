BEGIN PLOT /H1_1997_I424463/d(0.|1.|20)-x01-y01
LogY=1
XLabel=$p_\perp$ [GeV]
YLabel=$1/N \mathrm{d}n/\mathrm{d}p_\perp$ [1/GeV]
END PLOT

BEGIN PLOT /H1_1997_I424463/d(29|3.)-x01-y01
LogY=0
XLabel=$\eta$
YLabel=$1/N \mathrm{d}n/\mathrm{d}\eta$
END PLOT


BEGIN PLOT /H1_1997_I424463/d01-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [5, 50], x = [1,100]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d02-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [5, 10], x = [1,2]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d03-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [6, 10], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d04-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [10, 20], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d05-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [10, 20], x = [5,8]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d06-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [10, 20], x = [8,15]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d07-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [10, 20], x = [15,40]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d08-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [20, 50], x = [5,14]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d09-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [20, 50], x = [14,30]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d10-x01-y01
Title=Charged particle $p_\perp$ in $1.5 < \eta < 2.5$, $Q^2 = [20, 50], x = [30,100]\,10^{-4}$
END PLOT


BEGIN PLOT /H1_1997_I424463/d11-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [5, 50], x = [1,100]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d12-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [5, 10], x = [1,2]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d13-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [6, 10], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d14-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [10, 20], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d15-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [10, 20], x = [5,8]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d16-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [10, 20], x = [8,15]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d17-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [10, 20], x = [15,40]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d18-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [20, 50], x = [5,14]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d19-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [20, 50], x = [14,30]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d20-x01-y01
Title=Charged particle $p_\perp$ in $0.5 < \eta < 1.5$, $Q^2 = [20, 50], x = [30,100]\,10^{-4}$
END PLOT


BEGIN PLOT /H1_1997_I424463/d29-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [5, 50], x = [1,100]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d30-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [5, 10], x = [1,2]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d31-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [6, 10], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d32-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [10, 20], x = [2,5]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d33-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [10, 20], x = [5,8]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d34-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [10, 20], x = [8,15]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d35-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [10, 20], x = [15,40]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d36-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [20, 50], x = [5,14]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d37-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [20, 50], x = [14,30]\,10^{-4}$
END PLOT

BEGIN PLOT /H1_1997_I424463/d38-x01-y01
Title=Charged particle $\eta$ for $p_\perp > 1$ GeV, $Q^2 = [20, 50], x = [30,100]\,10^{-4}$
END PLOT
