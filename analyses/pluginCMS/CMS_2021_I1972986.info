Name: CMS_2021_I1972986
Year: 2021
Summary: Measurement and QCD analysis of double-differential inclusive jet cross sections in proton-proton collisions at 13 TeV
Experiment: CMS
Collider: LHC
InspireID: 1972986
Status: VALIDATED
Authors:
 - cms-pag-conveners-smp@cern.ch
 - Patrick L.S. Connor <patrick.connor@desy.de>
References:
  - arXiv:2111.10431
  - CMS-SMP-20-011
  - JHEP 02 (2022) 142
RunInfo: pp to jets at $\sqrt{s} = 13$ TeV. Data collected by CMS during the year 2016.
NeedCrossSection: yes
Beams: [p+, p+]
Energies: [13000]
Description:
  'A measurement of the inclusive jet production in proton-proton collisions at the LHC at √s = 13 TeV is presented. The double-differential cross sections are measured as a function of the jet transverse momentum pT and the absolute jet rapidity |y|. The anti-kT clustering algorithm is used with distance parameter of 0.4 (0.7) in a phase space region with jet pT from 97 GeV up to 3.1 TeV and |y|< 2.0. Data collected with the CMS detector are used, corresponding to an integrated luminosity of 36.3/fb (33.5/fb). The measurement is used in a comprehensive QCD analysis at next-to-next-to-leading order, which results in significant improvement in the accuracy of the parton distributions in the proton. Simultaneously, the value of the strong coupling constant at the Z boson mass is extracted as alpha_S(Z) = 0.1170 ± 0.0019. For the first time, these data are used in a standard model effective field theory analysis at next-to-leading order, where parton distributions and the QCD parameters are extracted simultaneously with imposed constraints on the Wilson coefficient c1 of 4-quark contact interactions.'
BibKey: CMS:2021yzl
BibTeX: '@article{CMS:2021yzl,
    author = "Tumasyan, Armen and others",
    collaboration = "CMS",
    title = "{Measurement and QCD analysis of double-differential inclusive jet cross sections in proton-proton collisions at $\sqrt{s}$ = 13 TeV}",
    eprint = "2111.10431",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CMS-SMP-20-011, CERN-EP-2021-221",
    month = "11",
    year = "2021"
}'
ReleaseTests:
 - $A LHC-13-DiJets-boosted
